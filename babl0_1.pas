{ This is an autogenerated unit using gobject introspection (gir2pascal). Do not Edit. }
unit Babl0_1;

{$MODE OBJFPC}{$H+}

{$PACKRECORDS C}
{$MODESWITCH DUPLICATELOCALS+}

{$LINKLIB libbabl-0.1-0.dll}
interface
uses
  CTypes;

const
  Babl0_1_library = 'libbabl-0.1-0.dll';

  BABL_ALPHA_FLOOR = 0;
  BABL_ALPHA_FLOOR_F = 0;
  BABL_MAJOR_VERSION = 0;
  BABL_MICRO_VERSION = 107;
  BABL_MINOR_VERSION = 1;

type
  TBablIccIntent = DWord;
const
  { BablIccIntent }
  BABL_ICC_INTENT_PERCEPTUAL: TBablIccIntent = 0;
  BABL_ICC_INTENT_RELATIVE_COLORIMETRIC: TBablIccIntent = 1;
  BABL_ICC_INTENT_SATURATION: TBablIccIntent = 2;
  BABL_ICC_INTENT_ABSOLUTE_COLORIMETRIC: TBablIccIntent = 3;
  BABL_ICC_INTENT_PERFORMANCE: TBablIccIntent = 32;

type
  TBablModelFlag = DWord;
const
  { BablModelFlag }
  BABL_MODEL_FLAG_ALPHA: TBablModelFlag = 2;
  BABL_MODEL_FLAG_ASSOCIATED: TBablModelFlag = 4;
  BABL_MODEL_FLAG_INVERTED: TBablModelFlag = 8;
  BABL_MODEL_FLAG_LINEAR: TBablModelFlag = 1024;
  BABL_MODEL_FLAG_NONLINEAR: TBablModelFlag = 2048;
  BABL_MODEL_FLAG_PERCEPTUAL: TBablModelFlag = 4096;
  BABL_MODEL_FLAG_GRAY: TBablModelFlag = 1048576;
  BABL_MODEL_FLAG_RGB: TBablModelFlag = 2097152;
  BABL_MODEL_FLAG_CIE: TBablModelFlag = 8388608;
  BABL_MODEL_FLAG_CMYK: TBablModelFlag = 16777216;

type
  TBablSpaceFlags = DWord;
const
  { BablSpaceFlags }
  BABL_SPACE_FLAG_NONE: TBablSpaceFlags = 0;
  BABL_SPACE_FLAG_EQUALIZE: TBablSpaceFlags = 1;
type
{ manual edit
  char* = pchar;
  char** = ppchar;
}
  double = cdouble;
  int = cint;
  long = clong;
  long_double = extended;
  size_t = csize_t;
  ulong = culong;
  unsigned_long_long = qword;
{ manual edit
  void* = pointer;
}
  Pvoid = pointer;
  gint  = int;
  Pgint = ^gint;

  PPBabl = ^PBabl;
  PBabl = ^TBabl;
  TBabl = record
  end;



  PPpchar = ^Ppchar;
  Ppchar = ^pchar;

  Pclong = ^clong;

  PPpointer = ^Ppointer;
  Ppointer = ^pointer;
  TBablFishProcess = procedure(babl: PBabl; src: Pchar; dst: Pchar; n: clong; data: Pvoid); cdecl;
  TBablFuncLinear = procedure(conversion: PBabl; src: Pchar; dst: Pchar; n: clong; user_data: Pvoid); cdecl;

  Pcint = ^cint;
  TBablFuncPlanar = procedure(conversion: PBabl; src_bands: cint; src: PPchar; src_pitch: Pgint; dst_bands: cint; dst: PPchar; dst_pitch: Pgint; n: clong; user_data: Pvoid); cdecl;

  PPBablIccIntent = ^PBablIccIntent;
  PBablIccIntent = ^TBablIccIntent;

  PPBablModelFlag = ^PBablModelFlag;
  PBablModelFlag = ^TBablModelFlag;

  PPBablSpaceFlags = ^PBablSpaceFlags;
  PBablSpaceFlags = ^TBablSpaceFlags;

function babl_component(name: Pchar): PBabl; cdecl; external;
function babl_component_new(first_arg: Pvoid; args: array of const): PBabl; cdecl; external;
function babl_conversion_get_destination_space(conversion: PBabl): PBabl; cdecl; external;
function babl_conversion_get_source_space(conversion: PBabl): PBabl; cdecl; external;
function babl_conversion_new(first_arg: Pvoid; args: array of const): PBabl; cdecl; external;
function babl_fast_fish(source_format: Pvoid; destination_format: Pvoid; performance: Pchar): PBabl; cdecl; external;
function babl_fish(source_format: Pvoid; destination_format: Pvoid): PBabl; cdecl; external;
function babl_fish_get_process(babl: PBabl): TBablFishProcess; cdecl; external;
function babl_format(encoding: Pchar): PBabl; cdecl; external;
function babl_format_exists(name: Pchar): cint; cdecl; external;
function babl_format_get_bytes_per_pixel(format: PBabl): cint; cdecl; external;
function babl_format_get_encoding(babl: PBabl): Pchar; cdecl; external;
function babl_format_get_model(format: PBabl): PBabl; cdecl; external;
function babl_format_get_n_components(format: PBabl): cint; cdecl; external;
function babl_format_get_space(format: PBabl): PBabl; cdecl; external;
function babl_format_get_type(format: PBabl; component_index: cint): PBabl; cdecl; external;
function babl_format_has_alpha(format: PBabl): cint; cdecl; external;
function babl_format_is_format_n(format: PBabl): cint; cdecl; external;
function babl_format_is_palette(format: PBabl): cint; cdecl; external;
function babl_format_n(type_: PBabl; components: cint): PBabl; cdecl; external;
function babl_format_new(first_arg: Pvoid; args: array of const): PBabl; cdecl; external;
function babl_format_with_space(encoding: Pchar; space: PBabl): PBabl; cdecl; external;
function babl_get_model_flags(model: PBabl): TBablModelFlag; cdecl; external;
function babl_get_name(babl: PBabl): Pchar; cdecl; external;
function babl_get_user_data(babl: PBabl): Pvoid; cdecl; external;
function babl_icc_get_key(icc_data: Pchar; icc_length: cint; key: Pchar; language: Pchar; country: Pchar): Pchar; cdecl; external;
function babl_icc_make_space(icc_data: Pchar; icc_length: cint; intent: TBablIccIntent; error: PPchar): PBabl; cdecl; external;
function babl_model(name: Pchar): PBabl; cdecl; external;
function babl_model_is(babl: PBabl; model_name: Pchar): cint; cdecl; external;
function babl_model_new(first_arg: Pvoid; args: array of const): PBabl; cdecl; external;
function babl_model_with_space(name: Pchar; space: PBabl): PBabl; cdecl; external;
function babl_new_palette(name: Pchar; format_u8: PPBabl; format_u8_with_alpha: PPBabl): PBabl; cdecl; external;
function babl_new_palette_with_space(name: Pchar; space: PBabl; format_u8: PPBabl; format_u8_with_alpha: PPBabl): PBabl; cdecl; external;
function babl_process(babl_fish: PBabl; source: Pvoid; destination: Pvoid; n: clong): clong; cdecl; external;
function babl_process_rows(babl_fish: PBabl; source: Pvoid; source_stride: cint; dest: Pvoid; dest_stride: cint; n: clong; rows: cint): clong; cdecl; external;
function babl_sampling(horizontal: cint; vertical: cint): PBabl; cdecl; external;
function babl_space(name: Pchar): PBabl; cdecl; external;
function babl_space_from_chromaticities(name: Pchar; wx: cdouble; wy: cdouble; rx: cdouble; ry: cdouble; gx: cdouble; gy: cdouble; bx: cdouble; by: cdouble; trc_red: PBabl; trc_green: PBabl; trc_blue: PBabl; flags: TBablSpaceFlags): PBabl; cdecl; external;
function babl_space_from_icc(icc_data: Pchar; icc_length: cint; intent: TBablIccIntent; error: PPchar): PBabl; cdecl; external;
function babl_space_from_rgbxyz_matrix(name: Pchar; wx: cdouble; wy: cdouble; wz: cdouble; rx: cdouble; gx: cdouble; bx: cdouble; ry: cdouble; gy: cdouble; by: cdouble; rz: cdouble; gz: cdouble; bz: cdouble; trc_red: PBabl; trc_green: PBabl; trc_blue: PBabl): PBabl; cdecl; external;
function babl_space_get_gamma(space: PBabl): cdouble; cdecl; external;
function babl_space_get_icc(babl: PBabl; length: Pgint): Pchar; cdecl; external;
function babl_space_is_cmyk(space: PBabl): cint; cdecl; external;
function babl_space_is_gray(space: PBabl): cint; cdecl; external;
function babl_space_with_trc(space: PBabl; trc: PBabl): PBabl; cdecl; external;
function babl_trc(name: Pchar): PBabl; cdecl; external;
function babl_trc_gamma(gamma: cdouble): PBabl; cdecl; external;
function babl_type(name: Pchar): PBabl; cdecl; external;
function babl_type_new(first_arg: Pvoid; args: array of const): PBabl; cdecl; external;
procedure babl_exit; cdecl; external;
procedure babl_gc; cdecl; external;
procedure babl_get_version(major: Pgint; minor: Pgint; micro: Pgint); cdecl; external;
procedure babl_init; cdecl; external;
procedure babl_introspect(babl: PBabl); cdecl; external;
procedure babl_palette_reset(babl: PBabl); cdecl; external;
procedure babl_palette_set_palette(babl: PBabl; format: PBabl; data: Pvoid; count: cint); cdecl; external;
procedure babl_set_user_data(babl: PBabl; data: Pvoid); cdecl; external;
procedure babl_space_get(space: PBabl; xw: Pdouble; yw: Pdouble; xr: Pdouble; yr: Pdouble; xg: Pdouble; yg: Pdouble; xb: Pdouble; yb: Pdouble; red_trc: PPBabl; green_trc: PPBabl; blue_trc: PPBabl); cdecl; external;
procedure babl_space_get_rgb_luminance(space: PBabl; red_luminance: Pdouble; green_luminance: Pdouble; blue_luminance: Pdouble); cdecl; external;
implementation
end.
